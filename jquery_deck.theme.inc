<?php

function template_preprocess_jquery_deck(&$vars) {
  drupal_add_css('sites/all/libraries/jquery_deck/core/deck.core.css');
  drupal_add_js('sites/all/libraries/jquery_deck/core/deck.core.js');

  // Allow to add possible extensions here.
  foreach ($vars['extensions'] as $extension) {
    drupal_add_js('sites/all/libraries/jquery_deck/extensions/' . $extension . '/deck.' . $extension . '.js');
    drupal_add_css('sites/all/libraries/jquery_deck/extensions/' . $extension . '/deck.' . $extension . '.css');
  }

  // @todo: Support style and transition.

  drupal_add_js(drupal_get_path('module', 'jquery_deck') . '/js/jquery_deck.js');

}

function template_process_jquery_deck(&$vars) {
  // Add the slides provided by views.
  if (isset($vars['rows'])) {
    $vars['slides'] = $vars['rows'];
  }
}