<?php

/**
 * Implements hook_views_plugins().
 */
function jquery_deck_views_plugins() {
  $plugins = array();
  $plugins['style']['jquery_deck'] = array(
    'title' => t('Jquery Deck Presentation'),
    'help' => t('Displays rows as an presentation.'),
    'handler' => 'jquery_deck_plugin_style_jquery_deck',
    'theme' => 'jquery_deck',
    'register theme' => FALSE,
    'uses row plugin' => TRUE,
    // @todo: Support this.
    'uses row class' => FALSE,
    'uses options' => TRUE,
    'type' => 'normal',
  );

  return $plugins;
}