<?php

class jquery_deck_plugin_style_jquery_deck extends views_plugin_style {
  function option_definition() {
    $options = parent::option_definition();
    // @todo: set up a valid default.
    $options['extensions'] = array('default' => array());
    $options['style'] = array('default' => '');
    $options['transition'] = array('default' => '');

    return $options;
  }


  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['extensions'] = array(
      '#title' => t('Extensions'),
      '#type' => 'checkboxes',
      '#options' => jquery_deck_get_extensions(),
      '#default_value' => $this->options['extensions'],
    );

    $form['style'] = array(
      '#title' => t('Style'),
      '#type' => 'radios',
      '#options' => jquery_deck_get_theme_styles() + array('' => t('Default'))
    );

    $form['transition'] = array(
      '#title' => t('Transition'),
      '#type' => 'radios',
      '#options' => jquery_deck_get_theme_transitions(),
      '#default_value' => $this->options['transition'],
    );
  }

  /**
   * Render the display in this style.
   */
  function render() {
    if ($this->uses_row_plugin() && empty($this->row_plugin)) {
      debug('views_plugin_style_default: Missing row plugin');
      return;
    }

    // Group the rows according to the grouping field, if specified.
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

    // Render each group separately and concatenate.  Plugins may override this
    // method if they wish some other way of handling grouping.
    $output = '';
    foreach ($sets as $title => $records) {
      if ($this->uses_row_plugin()) {
        $rows = array();
        foreach ($records as $row_index => $row) {
          $this->view->row_index = $row_index;
          $rows[$row_index] = $this->row_plugin->render($row);
        }
      }
      else {
        $rows = $records;
      }

      $output .= theme($this->theme_functions(),
        array(
          'view' => $this->view,
          'options' => $this->options,
          'rows' => $rows,
          'title' => $title,
          'extensions' => $this->options['extensions'],
          'style' => $this->options['style'],
          'transition' => $this->options['transition']
          )
        );
    }
    unset($this->view->row_index);
    return $output;
  }

}